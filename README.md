# About
An external python module for Project Reality that maps internal BF2 server structures to
enable easily modifying functions and values from within the game's python extensions
during runtime, primarily realitymemory.py. The library is meant to compile and work with [BF2CPython](https://github.com/yossizap/BF2-CPython).

This library has allowed us to add previously thought of as impossible features such as 
fastropes, body dragging, dynamic stamina and more.

# Building
To compile against the BF2 CPython executables and libraries:

## Linux
    NOTE that gcc 8 or higher is required for naked functions in x86. Do not ignore 'naked attribute ignored' warnings!
    mkdir build; cd build
    cmake .. \
        -DPYTHON_LIBRARY=<Path to BF2-CPython>/libpython2.7.so \
        -DPYTHON_INCLUDE_DIR=<Path to BF2-CPython>/Include/ \
        -DPYTHON_EXECUTABLE=<Path to BF2-CPython>/python \
 
## Windows
    
    mkdir build; cd build
    cmake .. \
        -DPYTHON_LIBRARY=<Path to BF2-CPython>/PCBuild/Python27.lib \
        -DPYTHON_INCLUDE_DIR=<Path to BF2-CPython>/Include/ \
        -DPYTHON_EXECUTABLE=<Path to BF2-CPython>/PCBuild/python.exe \
        -A"Win32" \

Prebuilt BF2CPython binaries can be obtained from [BF2Cpython releases page](https://github.com/yossizap/BF2-CPython/releases).

The module can also be compiled against regular python on both platforms with the
following commands:

    mkdir build; cd build
    cmake ..