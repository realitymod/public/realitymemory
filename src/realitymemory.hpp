#ifndef REALITYMEMORY_H
#define REALITYMEMORY_H

#include <string>
#include <pybind11/functional.h>
#include <pybind11/pybind11.h>
//#include "ObjectCreatedHook.hpp"
//#include "ServerGiveDamage.hpp"
#include "ObjectManager.hpp"
#include "FireComp-CreateProjectile.hpp"
#include "ObjectSpawner-SpawnObject.hpp"
#include "AmmoComp-refillPercent.hpp"
#include "SelectEmptySeat.hpp"
#include "DisableAALatencyCompensation.hpp"
#include "Soldier-bulletFlyBy.hpp"
#include "hook.hpp"
#include "GameServer-ExitVehicle.hpp"
#include "Player-setSpawnGroup.hpp"
#include "SoldierRecoilComp-getRecoil.hpp"
#include "SoldierFrameSkipFix.h"
#include "onlyZoomWhenProne.h"

#include "ObjectManager.hpp"


#ifdef WIN32
	extern void* getRootParent;
	extern void* rotationalBundle__applyRotation;
	extern void* python__wrapObject;
	extern void* object__updateFlags; // ecx = this, arg0 bits to add, arg4 bits to remove

	#define	BF2PYTHON_WRAPOBJECTTO(cobject, target) __asm \
	{\
		__asm mov ecx, cobject \
		__asm mov eax, python__wrapObject \
		__asm call eax \
		__asm mov target, eax \
	}
		

#else
	extern void* (*python__wrapObject)(void*);
	extern void* (*getRootParent)(void*);
	// BF2 returns pointer to PyObject. The object is already ref counted. This tells our py::object / py::handle to not increase ref count.
	#define BF2PYTHON_WRAPOBJECTTO(cobject, target) target = py::object(py::handle((PyObject * )python__wrapObject(cobject)), false);
#endif

void printConsole(std::string str);

#endif  // REALITYMEMORY_H
