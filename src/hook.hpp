#ifndef HOOK_H
#define HOOK_H

#include <cstring>
#include <pybind11/functional.h>
#include <pybind11/pybind11.h>
namespace py = pybind11;

#ifdef WIN32
#include <Windows.h>
void overwriteVTableEntry(void *entryPos, void* newFunction);
bool overwriteCall(void *callposition, void* newFunction);
void injectJump(void *pos, void *target);
void injectFarJump(void *pos, void *target); // 5 bytes offset jump
#else
#include <sys/mman.h>
void overwriteVTableEntry(void* entryPos, void* newFunction);
void injectJump(void* pos, void* target);
void injectJumpRBX(void* pos, void* target);
#endif

void writeProtectedPy(py::int_ address, py::bytes data);

#endif  // HOOK_H