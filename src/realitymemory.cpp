#include "realitymemory.hpp"

namespace py = pybind11;
py::object host;
py::object rcon_invoke;

void printConsole(std::string str)
{
    py::object res = rcon_invoke.call(py::bytes("echo \"" + str + "\""));
}
#ifdef WIN32
	void* python__wrapObject = (void*)0x00535730;
	void* getRootParent = (void*)0x0006D8DF0;
	void** squadManagerP = (void**)0x0083BF54;
	void* squadManagerResetSquad = (void*)0x0045FE40;
	void* object__updateFlags = (void*)0x006DBC20;
	void* rotationalBundle__applyRotation = (void*)0x006224A0;
#else
	void* (*python__wrapObject)(void*) = (void* (*)(void*))0x004F9250;
	void* (*getRootParent)(void*) = (void* (*)(void*))0x069A1E0;
	void** squadManagerP = (void**)0x010b8c00;
	void (*squadManagerResetSquad)(void*, int, int) = (void (*)(void*, int, int))0x004C1590;
	void (*object__updateFlags)(void*, int, int) = (void (*)(void*, int, int))0;
#endif

//#ifdef WIN32
//	void _resetSquads(void* squadManager, int team, int squad) {
//		__asm {
//			push ebp;
//			mov ebp, esp;
//			sub esp, 0x30; 
//			pushad; 
//
//
//			mov ecx, squadManager;
//			push squad;
//			push team;
//			mov eax, squadManagerResetSquad;
//			call eax;
//
//
//			popad; 
//			mov esp, ebp;
//			pop ebp;
//			ret 8;  
//		}
//
//	}
//#endif

// rdi, esi, edx	
	void resetSquads() {
#ifdef WIN32
		void* squadManager = *squadManagerP;
		for (int team = 1;team <= 2;team++)
			for (int squad = 1;squad <= 9;squad++)
			{
				__asm {
					mov ecx, squadManager;
					push squad;
					push team;
					mov eax, squadManagerResetSquad;
					call eax;
				}

			}
#else
		for (int team=1;team<=2;team++)
			for (int squad=1;squad<=9;squad++)
				squadManagerResetSquad(*squadManagerP, team, squad);
#endif
	}


#ifdef WIN32
	// Inject to 0x0054C2D1
	__declspec(naked) void relevantObjectsRootVehicle() {
		__asm {
			mov eax, getRootParent
			call eax;
			mov edx, [eax];
			mov ecx, eax;
			call dword ptr[edx + 0xD4];
			mov ecx, eax;
			mov eax, 0x0054C2D9;
			jmp eax;
		}
	}
#else
#endif

py::object wrapBF2ObjectToPython(py::int_ bf2objpointer) {
	// py int -> value -> cast to pointer of size
	void* bf2ptr = (void*)(unsigned long)bf2objpointer;

	py::object o = py::none();
	if (bf2ptr)
	{
		BF2PYTHON_WRAPOBJECTTO(bf2ptr, o);
	}
	return o;
}

void updateFlags(py::int_ bf2objpointer, py::int_ add, py::int_ remove) {
	void* bf2ptr = (void*)(unsigned long)bf2objpointer;
	unsigned int toAdd = add;
	unsigned int toRemove = remove;


#ifdef WIN32
	__asm {
		mov ecx, bf2ptr;
		push toAdd;
		push toRemove;
		mov eax, object__updateFlags;
		call eax;
	}
#else
#endif


}

#ifdef WIN32
void rotbundleApplyRotation(py::int_ bf2objpointer, py::float_ x, py::float_ y, py::float_ z)
{
	void* bf2ptr = (void*)(unsigned long)bf2objpointer;
	float* vec = (float*)(((char*)bf2ptr) + 0x158);

	vec[0] = x;
	vec[1] = y;
	vec[2] = z;

	__asm {
		mov ecx, bf2ptr;
		push vec;
		mov eax, rotationalBundle__applyRotation;
		call eax;
	}
}
#else
void rotbundleApplyRotation(py::int_ bf2objpointer, py::float_ x, py::float_ y, py::float_ z) {
	// TODO
}
#endif





#ifdef WIN32
#define ServerGameLogic (0x83BF50)
#define ServerGameLogic__HandlePickup (0x455450)
void PickUpKit(py::int_ objpointerkit, py::int_ objpointerplayer, py::int_ objpointersoldier) {

	void* ptrkit = (void*)(unsigned long)objpointerkit;
	void* ptrplayer =(void*)(unsigned long)objpointerplayer;
	void* ptrsoldier = (void*)(unsigned long)objpointersoldier;

	__asm {
		push 1;
		push ptrsoldier;
		push ptrplayer;
		push ptrkit;
		mov ecx, DWORD PTR [ServerGameLogic];
		mov eax, ServerGameLogic__HandlePickup;
		call eax;
	}


}
#else
#define ServerGameLogic (0x10B8C08)
#define ServerGameLogic__HandlePickup (0x4A6520)
void (*handlePickup)(void*, void*, void*, void*, bool) = (void (*)(void*, void*, void*, void*, bool))ServerGameLogic__HandlePickup;
void PickUpKit(py::int_ objpointerkit, py::int_ objpointerplayer, py::int_ objpointersoldier) {

	void* ptrkit = (void*)(unsigned long long)objpointerkit;
	void* ptrplayer = (void*)(unsigned long long)objpointerplayer;
	void* ptrsoldier = (void*)(unsigned long long)objpointersoldier;
	void* gamelogic = *(void**)ServerGameLogic;
	handlePickup(gamelogic, ptrkit, ptrplayer, ptrsoldier, 1);
}

#endif


PYBIND11_MODULE(_realitymemory, m) {

	//m.def("wrapBF2ObjectToPython", &wrapBF2ObjectToPython, py::return_value_policy::take_ownership,
	//	R"pbdoc(
 //       Wrap BF2 object to python
 //   )pbdoc");

	m.def("ServerGameLogic_HandlePickup", PickUpKit,
		R"pbdoc(
        Initialize projectile created hook
    )pbdoc");

	m.def("ObjectManager_PostMessage", &ObjectManager::postMessage_py,
		R"pbdoc(
        Post a message to an object. Attention: This can crash servers horribly!
    )pbdoc");


	m.def("initializeProjectileCreatedHook", &FireCompCreateProjectile::initializeHook,
           R"pbdoc(
        Initialize projectile created hook
    )pbdoc");

	m.def("addPreProjectileCreatedTemplate",
              &FireCompCreateProjectile::addPreProjectileCreatedTemplate,
              R"pbdoc(
        Add a weapon template name to callback pre-fire
    )pbdoc");

	m.def("addProjectileCreatedTemplate", 
			&FireCompCreateProjectile::addProjectileCreatedTemplate,
              R"pbdoc(
        Add a weapon template name to callback post-fire
    )pbdoc");

	//m.def("initializeSoldierBulletFlyByHook", &BulletFlyBy::initializeHook, R"pbdoc(
 //       Initialize bulletflyby hook
 //   )pbdoc");

	//m.def("addBulletFlyByTemplate", &BulletFlyBy::addTemplate, R"pbdoc(
 //       Add template to bulletFlyBy hook
 //   )pbdoc");


	m.def("rotbundleApplyRotation", &rotbundleApplyRotation,
		R"pbdoc(
        Call RotationalBundle::ApplyRotation(rotbundle, vec3&)
    )pbdoc");

	m.def("initializeObjectSpawnerHook", &ObjectSpawnerSpawnObject::initializeHook,
		R"pbdoc(
        Initialize Object Spawner hook
    )pbdoc");

	m.def("addObjectSpawnerTemplate",
		&ObjectSpawnerSpawnObject::addTemplate,
		R"pbdoc(
        Add a template to the object Spawner callback filter
    )pbdoc");


	m.def("initializeAmmoCompRearmHook",
		AmmoCompRefillPerecnt::initializeHook,
		R"pbdoc(
        init rearm hook
    )pbdoc");

	m.def("updateFlags",
		updateFlags,
		R"pbdoc(
        init rearm hook
    )pbdoc");


	//m.def("initializeServerGameLogicGiveDamageHook",
	//	ServerGameLogicGiveDamage::initializeHook,
	//	R"pbdoc(
 //       init givedamage hook
 //   )pbdoc");


	m.def("forceExitVehicle",
		GameServerExitVehicle::ExitVehicle,
		R"pbdoc(
    )pbdoc");


	m.def("initializeSelectEmptySeatHook",
		SelectEmptySeat::initializeHook,
		R"pbdoc(
    )pbdoc");

	m.def("initializeExplosionLagCompHook",
		AALagComp::initializeHook,
		R"pbdoc(
    )pbdoc");


	m.def("initializeSetSpawnGroupHook",
		SetSpawnGroup::initializeHook,
		R"pbdoc(
    )pbdoc");


	
	m.def("initializeSoldierFrameSkipHook",
              SoldierFrameSkipFix::initializeHook,
              R"pbdoc(
    )pbdoc");

	m.def("initializeOnlyZoomWhenProneHook",
              OnlyZoomWhenProne::initializeHook,
              R"pbdoc(
    )pbdoc");
	

	m.def("explosionLagCompOverrideTemplate",
		AALagComp::addTemplate,
		R"pbdoc(
    )pbdoc");

	m.def("resetSquads",
		&resetSquads,
		R"pbdoc(
        Reset all squads
    )pbdoc");
	

	m.def("writeProtected",
		&writeProtectedPy,
		R"pbdoc(
			Write protected memory and restore permissions afterwards
		)pbdoc"
	);

			

	m.def("initializeGetRecoilHook", GetRecoilHook::initializeHook,
           R"pbdoc(
        Initialize getRecoil hook
    )pbdoc");

	m.doc() = R"pbdoc(
		Realitymemory plugin
		--------------------

		.. currentmodule:: realitymemory

		.. autosummary::
            :toctree: _generate

		    init
	)pbdoc";

	host = py::module::import("host");
	rcon_invoke = host.attr("rcon_invoke");

	//injectJump((void*)0x0054C2D1, relevantObjectsRootVehicle);


#ifdef VERSION_INFO
    m.attr("__version__") = VERSION_INFO;
#else
    m.attr("__version__") = "dev";
#endif






}
