#ifndef BULLETFLYBY__H
#define BULLETFLYBY__H

#include <pybind11/functional.h>
#include <pybind11/pybind11.h>
#include "hook.hpp"
#include "utils.hpp"

namespace py = pybind11;
namespace BulletFlyBy
{
	void addTemplate(py::bytes name);
	void initializeHook(py::function _callback);
}




#endif
