#include "FireComp-CreateProjectile.hpp"




namespace FireCompCreateProjectile
{
std::set<std::string> preProjectileCreatedTemplates;
std::map<std::string, std::set<py::function> > projectileCreatedTemplates;

void addPreProjectileCreatedTemplate(py::bytes name)
{
    std::string lower = name;
    tolower(&lower);
    
    preProjectileCreatedTemplates.insert(lower);
}

void addProjectileCreatedTemplate(py::bytes name, py::function callback)
{
    std::string lower = name;
    tolower(&lower);
    projectileCreatedTemplates[lower].insert(callback);
}

bool initialized = false;

py::object precallback;

#ifdef WIN32
__declspec(naked) void __stdcall hook()
{
    // Backup everything
    __asm {
      push ebp;
      mov ebp, esp;  
	  sub esp, __LOCAL_SIZE;  // room for locals
	  pushad;  // save registers
    }

    void *weapon;
    void *projectile;
    void *fireComp;
    float *transformationMatrix;
    float *velocityVector;
    // store fireComp
    __asm {
		  mov fireComp, ecx;

		  mov eax, [ebp + 0x08]  // arg_0 = float* (mat4x4) transformation
		  mov transformationMatrix, eax;

		  mov eax, [ebp + 0x0C]  // arg_4 = float* (vec3) velocity
		  mov velocityVector, eax;

		  mov eax, [ecx + 0x0C];  // Fire component(this) + 0x0C = pointer to
                                // owning GenericFireArm
		  mov weapon, eax;
    }

    askPythonWhereToSpawn(weapon, transformationMatrix, velocityVector);

    // Call original, store result
    _asm
    { 
		mov ecx, fireComp;
		push[ebp + 0x0C];  // velocity vec3
        push[ebp + 0x08];  // transformation matrix
        mov eax, 0x005C0770
        call eax;
        mov projectile, eax;
    }

    // Call python
    callCallback(weapon, projectile);

    // epilogue
    __asm {
	  popad;  // restores registers
	  mov eax, projectile;  // RETURN THE CREATED PROJECTILE!
      mov esp, ebp;
      pop ebp;
      ret 8;  // pop args
    }
}



#else
__attribute__((naked, noinline, optimize("O0"))) void *calloriginal(volatile void *firecomp,
	volatile float *transformationMatrix,
	volatile float *velocityVector)
{	


    asm("push r15");
    asm("push r14");
    asm("push r13");
    asm("push r12");
    asm("push rbp");
    asm("mov r15, rsi");
    asm("mov rax, 0x005F638C");
    asm("jmp rax");

}

__attribute__((optimize("O0"))) void *hook_linux(void *firecomp,
		float *transformationMatrix,
		float *velocityVector)
{
	void *weapon = *(void**)((char *)(firecomp) + 0x10);


	// pre-setup projectile event
	askPythonWhereToSpawn(weapon, transformationMatrix, velocityVector);



    // Call original, store result
    void *projectile = calloriginal(
        (volatile void*)firecomp, 
		(volatile float*)transformationMatrix,
		(volatile float*)velocityVector);
	

    // Call python
    callCallback(weapon, projectile);

	return projectile;
}


#endif


void askPythonWhereToSpawn(void *weapon,
                           float *transformationMatrix,
                           float *velocity) noexcept
{
    py::object pyObjectWeapon;
	std::string name = getTemplateName(weapon);

    if (preProjectileCreatedTemplates.count(name) == 0)
        return;


	BF2PYTHON_WRAPOBJECTTO(weapon, pyObjectWeapon)

    py::list pyVelocity;
    py::list mat;

    for (int i = 0; i < 3; i++)
        pyVelocity.append(py::float_(velocity[i]));
    for (int i = 0; i < 16; i++)
        mat.append(py::float_(transformationMatrix[i]));

    py::object res = precallback.call(pyObjectWeapon, mat, pyVelocity);

    for (int i = 0; i < 16; i++)
        transformationMatrix[i] = mat[i].cast<float>();
    for (int i = 0; i < 3; i++)
        velocity[i] = pyVelocity[i].cast<float>();
}

void callCallback(void *weapon, void *projectile) noexcept
{
    py::object pyObjectWeapon, pyObjectProjectile;
    auto it = projectileCreatedTemplates.find(getTemplateName(projectile));
    if (it == projectileCreatedTemplates.end())
        return;

	BF2PYTHON_WRAPOBJECTTO(projectile, pyObjectProjectile)
	BF2PYTHON_WRAPOBJECTTO(weapon, pyObjectWeapon)


    for (auto pyFunc : (*it).second) {
        py::object res = pyFunc.call(pyObjectWeapon, pyObjectProjectile);
    }
}

void initializeHook(py::function _callback)
{
    if (initialized)
        return;

    initialized = true;

    precallback = _callback;
    

#ifdef WIN32
	overwriteCall((void *)0x005C0F17, &hook);
	overwriteCall((void *)0x005A32D0, &hook);
#else
	injectJump((void*)0x005F6380, (void*)&hook_linux);
#endif
}

}  // namespace FireCompCreateProjectile