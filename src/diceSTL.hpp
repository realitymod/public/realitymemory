#pragma once
#ifndef DICESTL_H
#define DICESTL_H
#include "utils.hpp"

namespace diceSTL
{
#define STLVector_FOREACH(vector, type, iterator)  for (type* iterator = (type*)vector->start; iterator < vector->end; iterator++)


	#define vecint32_resize (0x00407B20)
	template<typename T>
	class STLVector {
	public:
#ifdef WIN32
		void* junk;
#endif
		T* start;
		T* end;
		T* buffer;

		
		unsigned int size() {
			return (T*)end - (T*)start;
		}

		T operator[](unsigned int index) {
			assert(index < size());

			T* s = (T*)start;
			return s[index];
		}


		//void resize(int size, int value);
	};
};

#endif