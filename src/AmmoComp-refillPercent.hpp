#ifndef AMMOCOMPREFILLPERCENT__H
#define AMMOCOMPREFILLPERCENT__H


#include <pybind11/functional.h>
#include <pybind11/pybind11.h>
#include <set>
#include <map>
#include "bf2_structs.hpp"
#include "realitymemory.hpp"
#include "utils.hpp"
#include "hook.hpp"

namespace py = pybind11;
namespace AmmoCompRefillPerecnt
{
	void initializeHook();
}

#endif