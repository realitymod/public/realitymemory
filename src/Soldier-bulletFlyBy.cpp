#include "Soldier-bulletFlyBy.hpp"

namespace BulletFlyBy
{
    #define BULLETFLYBY_CALLBACK_COOLDOWN (0.7f)
    #define MINIMUM_BULLET_ALIVE_TIME (0.05f)
    

    py::function callback;
    std::set<std::string> templates;
    std::map<void*, float> lastTick;

    void addTemplate(py::bytes name)
    {
        std::string lower = name;
        tolower(&lower);
        templates.insert(lower);
    }


    void hooklogic(void* isoldier, void* bullet) {

        float time = *((double*)WORLD_TIME);
        if (time - ((CGenericProjectile*)bullet)->spawnTime < MINIMUM_BULLET_ALIVE_TIME)
            return;
        std::string bullettemplate = getTemplateName(bullet);
        if (templates.count(bullettemplate) == 0)
            return;

       
        // Cast from ISoldier interface to Object
#ifdef WIN32
        void* soldier = (((char*)isoldier) - 0x15C);      
#else
        void* soldier = isoldier;
#endif

        auto iter = lastTick.find(soldier);
        if (iter != lastTick.end() && (*iter).second + BULLETFLYBY_CALLBACK_COOLDOWN > time)
            return;
        lastTick[soldier] = time;
          

        // python wrap
        py::object pySoldier;
        BF2PYTHON_WRAPOBJECTTO(soldier, pySoldier);

        py::bytes pyBullettemplate = bullettemplate;

        // Don't wrap bullet, if we need any logic dependant on bullet instance it should be done in C
        callback.call(pySoldier, pyBullettemplate);
    }

#ifdef WIN32

    // ecx - this soldier (ISoldier interface, NOT Object!)
    // arg0 - bullet
    // arg4 - vec3
    // arg8 - vec3
    // Callee cleanup expected (size 0x0C)
    __declspec(naked) void hookwindows() {
        void* isoldier; // ISoldier interface
        void* bullet;
        // We do not need to jump to default code, it does iirelevant sound-related stuff that were compiled into server unintentionally
        __asm {
            push    ebp;
            mov     ebp, esp;
            sub esp, __LOCAL_SIZE;

            mov isoldier, ecx;

            mov eax, [ebp + 0x08];
            mov bullet, eax;
        }
        hooklogic(isoldier, bullet);

        __asm {
            mov     esp, ebp;
            pop     ebp;
            retn 0x0C;
        }
    }
#else
    void hooklinux(void* isoldier, void* bullet) 
    {
        hooklogic(isoldier, bullet);
    }
#endif

    // Don't have to worry about clearing deleted soldiers from lastTick midround, 
    // game reuses the objects  so when a soldier is "deleted" the new one will have the same pointer.
    void onStatusChange(py::object status)
    {
        lastTick.clear();
    }


    void initializeHook(py::function _callback)
    {
        callback = _callback;
#ifdef WIN32
        injectJump((void*)0x0054FD10, (void*)&hookwindows);
#else
        injectJump((void*)0x0546E50, (void*)&hooklogic);
#endif


        py::object host = py::module::import("host");
        py::object registerGameStatusHandler = host.attr("registerGameStatusHandler");
        registerGameStatusHandler.call(std::function<void(py::object)>(&onStatusChange));


    }

}

