#ifndef FIRECOMPCREATEDPROJECTILE__H
#define FIRECOMPCREATEDPROJECTILE__H

#include <pybind11/functional.h>
#include <pybind11/pybind11.h>
#include <set>
#include <map>
#include "bf2_structs.hpp"
#include "realitymemory.hpp"
#include <stdio.h>
#include <string.h>
#include "hook.hpp"
#include "utils.hpp"




namespace py = pybind11;
namespace FireCompCreateProjectile
{	
	//extern std::set<std::string> templates;

	void addPreProjectileCreatedTemplate(py::bytes name);
	void addProjectileCreatedTemplate(py::bytes name, py::function callback);

    void initializeHook(py::function callback);
        
    //py::object registerTemplateName(py::object objectType, py::object templat);
    void callCallback(void *weapon, void *iObject) noexcept;
    void askPythonWhereToSpawn(void *weapon,
                               float *transformationMatrix,
                               float *velocity) noexcept;
	

#if WIN32
#else
	void* hook_linux(void* firecomp,
		float* transformationMatrix,
		float* velocityVector);
#endif

}  // namespace CreateProjectile

#endif  // FIRECOMPCREATEDPROJECTILE__H
