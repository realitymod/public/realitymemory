#include "Player-setSpawnGroup.hpp"

namespace SetSpawnGroup
{
    py::function pyCallback;
#ifdef WIN32
    void __fastcall callback(int playerid, int group) {
#else
    void callback(int playerid, int group) {
#endif
        pyCallback.call(playerid, group);
    }



#ifdef WIN32
    __declspec(naked) void* hook() {
        __asm {
            mov     eax, [esp + 4];
            mov     [ecx + 0xF0], eax; //spawngroup

            mov ecx, [ecx + 0xB8];  //player id
            mov edx, eax; // spawngroup
            call callback;
            ret 4;
        }

    }
#else
    void hook(void* player, int spawngroupid) {
        // set spawngroup id
        *((int*)(((char*)player) + 0x110)) = spawngroupid;
        // get id for callback
        int id = *((int*)(((char*)player) + 0xD4));
        callback(id, spawngroupid);
    }

#endif

    void initializeHook(py::function _callback)
    {
        pyCallback = _callback;
#ifdef WIN32
        overwriteVTableEntry((void*)0x00792C48, &hook);
#else
        overwriteVTableEntry((void*)0x0B35058, (void*)&hook);
#endif

    }

}

