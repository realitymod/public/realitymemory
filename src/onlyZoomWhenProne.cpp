#include "onlyZoomWhenProne.h"
#include "bf2classes.hpp"
#include "hook.hpp"

using namespace dice;
namespace OnlyZoomWhenProne
{
#ifdef _WIN32
#define I_OBJ_COMP_PTR(a) ((_CDefaultZoomComp *)((uintptr_t)(a)-0x10))
#else
#define I_OBJ_COMP_PTR(a) ((_CDefaultZoomComp *)((uintptr_t)(a)-0x20))
#endif

#ifdef _WIN32
bool __fastcall ZoomComp_canZoom(CDefaultZoomComp *_this)
{
#else
bool ZoomComp_canZoom(_CDefaultZoomComp *__this)
{
    CDefaultZoomComp *_this = &__this->_DefaultZoomComp;
#endif
    if (!_this->temp->onlyZoomWhenProne)
    {
        return true;
    }

    if (_this->zoomFactorIndex)
    {  // allow unzoom regardles of pose
        return true;
    }

    CGenericFireArm *weapon =
        &I_OBJ_COMP_PTR(_this)->weaponObject->_GenericFirearm;
    CPlayer *player = weapon->getPlayer();
    if (!player)
        return false;
    CObject *pco = player->getVehicle();
    if (!pco)
        return false;
    if (pco->objectTemplate->getClassID() == CID_SoldierTemplate)
    {
        CSoldier *soldier = &((_CSoldier *)pco)->_Soldier;
        if (_this->temp->onlyZoomWhenProne &&
            soldier->getPose() == SoldierPose::Prone)
        {
            return true;
        }
    }
    return false;
}

#ifndef _WIN32
float non_virtual_thunk_to_ZoomComp_canZoom(_CDefaultZoomComp *_this)
{
    return ZoomComp_canZoom(I_OBJ_COMP_PTR(_this));
}
#endif

#ifdef _WIN32
#define O_DEFAULT_ZOOM_COMP__UPDATE_COMPONENT_PTR 0x5A5B60
#define O_DEFAULT_ZOOM_COMP__CHANGE_ZOOM_PTR 0x5A5CB0
#endif

#ifdef _WIN32
void(__thiscall *DefaultZoomComp__changeZoom)(_CDefaultZoomComp *, int, bool) =
    (void(__thiscall *)(_CDefaultZoomComp *, int, bool))
        O_DEFAULT_ZOOM_COMP__CHANGE_ZOOM_PTR;
void(__thiscall *DefaultZoomComp__updateComponent)(CDefaultZoomComp *,
                                                   float,
                                                   unsigned int) =
    (void(__thiscall *)(CDefaultZoomComp *, float, unsigned int))
        O_DEFAULT_ZOOM_COMP__UPDATE_COMPONENT_PTR;
#else
void (*DefaultZoomComp__changeZoom)(_CDefaultZoomComp *, int, bool) =
    (void (*)(_CDefaultZoomComp *, int, bool))0x629990;
void (*DefaultZoomComp__updateComponent)(_CDefaultZoomComp *,
                                         float,
                                         unsigned int) =
    (void (*)(_CDefaultZoomComp *, float, unsigned int))0x6297E0;
#endif

#ifdef _WIN32
void __fastcall DefaultZoomComp__updateComponent_hook(CDefaultZoomComp *_this,
                                                      int,
                                                      float f,
                                                      unsigned int t)
{
    _CDefaultZoomComp *__this = I_OBJ_COMP_PTR(_this);
    DefaultZoomComp__updateComponent(_this, f, t);
#else
void DefaultZoomComp__updateComponent_hook(_CDefaultZoomComp *__this,
                                           float f,
                                           unsigned int t)
{
    CDefaultZoomComp *_this = &__this->_DefaultZoomComp;
    DefaultZoomComp__updateComponent(__this, f, t);
#endif

    if (_this->zoomFactorIndex && _this->temp->onlyZoomWhenProne)
    {
        CGenericFireArm *weapon = &__this->weaponObject->_GenericFirearm;
        CPlayer *player = weapon->getPlayer();
        if (!player)
            return;
        CObject *pco = player->getVehicle();
        if (!pco)
            return;
        if (pco->objectTemplate->getClassID() == CID_SoldierTemplate)
        {
            CSoldier *soldier = &((_CSoldier *)pco)->_Soldier;
            if (soldier->getPose() != SoldierPose::Prone)
            {
                DefaultZoomComp__changeZoom(__this, 0, false);
            }
        }
    }
}

#ifndef _WIN32
void non_virtual_thunk_to__DefaultZoomComp_updateComponent(
    CDefaultZoomComp *_this,
    float f,
    unsigned int t)
{
    return DefaultZoomComp__updateComponent_hook(I_OBJ_COMP_PTR(_this), f, t);
}
#endif

void initializeHook()
{
#ifdef _WIN32
    overwriteVTableEntry((void *)0x7D8ACC, ZoomComp_canZoom);
    overwriteVTableEntry((void *)0x7D8A38, DefaultZoomComp__updateComponent_hook);
#else
    overwriteVTableEntry((void *)0xB7FC08, (void*)ZoomComp_canZoom);
    overwriteVTableEntry((void *)0xB7FD98, (void*)non_virtual_thunk_to_ZoomComp_canZoom);
    overwriteVTableEntry((void *)0xB7FB00, (void*)DefaultZoomComp__updateComponent_hook);
    overwriteVTableEntry((void *)0xB7FC68, (void*)non_virtual_thunk_to__DefaultZoomComp_updateComponent);
#endif
}
}  // namespace OnlyZoomWhenProne