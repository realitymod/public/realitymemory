#include "DisableAALatencyCompensation.hpp"

namespace AALagComp
{
    std::set<std::string> templates;

#ifdef WIN32
    int** g_explosionLatencyComp = (int**)(0x0886DE0);
#else
    int** g_explosionLatencyComp = (int**)(0x108CCE0);
#endif

    void addTemplate(py::bytes name) 
    {
        std::string lower = name;
        tolower(&lower);
        templates.insert(lower);
    }


#ifdef WIN32
    void __stdcall check(void* obj) {
#else
    __attribute__((noinline, optimize("O0")))void check(void* obj) {
#endif
        
        if (templates.count(getTemplateName(obj)) != 0) {
            //printConsole("Disabled for: " + getTemplateName(obj));
            **g_explosionLatencyComp = 0;
            // Set it to die next tick
            ObjectManager::postMessage(obj, MESSAGE_SET_TTL, 2.0f/30.0f);
        }
            
        else
            **g_explosionLatencyComp = 1;
    }


#ifdef WIN32
    __declspec(naked) void hookwindows() {

        __asm {
            push  ebp;
            mov   ebp, esp;
            pushad;

            push[ebp + 0x28];
            call check;


            popad;
            sub   esp, 0x90;
            mov   eax, 0x0056DDB9;
            jmp   eax;
        }


    }
#else

    __attribute__((naked, noinline, optimize("O0")))void hooklinux() {
        register void* obj asm("rdi");


        // backup callee registers as we overwritten the original's backup
        asm("push    r15");
        asm("push    r14");
        asm("push    r13");
        asm("push    r12");
        asm("push    rbp");
        asm("push    rbx");




        // backup before the xchg
        asm("push rdi");
        // Fuck calculating stack offsets for parameters
        // R13 has the parameter we need, parent (only caller) uses it to move to stack
        // Original code expects a rdi -> r13 move
        // We need a r13 -> rdi move
        asm("xchg r13, rdi");
        // backup registers that original may expect to use because we're at the start of the function and these are the arguments
        asm("push rsi");
        asm("push rdx");
        asm("push rcx");
        asm("push r8");
        asm("push r9");
        //backup xmm0-3 because they're used as args
        asm("sub     rsp, 0x10");
        asm("movss [rsp+0x0C], xmm0");
        asm("movss [rsp+0x08], xmm1");
        asm("movss [rsp+0x04], xmm2");
        asm("movss [rsp+0x00], xmm3");



        check(obj); // apply setting

        
        asm("movss xmm0, [rsp+0x0C]");
        asm("movss xmm1, [rsp+0x08]");
        asm("movss xmm2, [rsp+0x04]");
        asm("movss xmm3, [rsp+0x00]");
        asm("add   rsp, 0x10");
        // pop backup and jump
        asm("pop r9");
        asm("pop r8");
        asm("pop rcx");
        asm("pop rdx");
        asm("pop rsi");
        asm("pop rdi");

        asm("mov rax, 0x4917BD");
        asm("jmp rax");

    }
#endif


    void initializeHook()
    {
#ifdef WIN32
        injectJump((void*)0x56DDB0, (void*)&hookwindows);
#else
        injectJump((void*)0x4917B0, (void*)&hooklinux);
#endif
    }

}

