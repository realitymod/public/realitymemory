#include "utils.hpp"


void tolower(std::string* s) {
	std::transform(s->begin(), s->end(), s->begin(),
		[](unsigned char c) { return std::tolower(c); }
	);
}

#if WIN32
std::string getTemplateName(void* obj)
{
	if (!obj)
		return std::string("");

    void *templat = ((CObject*)obj)->obj_template;

	
    const char* data = cstr_from_std_string((pSTD71_STRING)POINTER_ADD(templat, 0x0C));
    std::string lower = std::string(data);
	tolower(&lower);
	return lower;

	// If this fails, template + 0x10 is the char* when string is long. template + 0x20 is string length, template + 0x24 is buffer length
	// sizeof std::string RELEASE is 0x18. DEBUG is 0x24. It seems to fit RELEASE with 0x10 short-string-optimization buffer
}
const char *cstr_from_std_string(pSTD71_STRING str)
{
    static_assert(sizeof(STD71_STRING) == 0x1C, "std71 string incorrect size");

    int buffersize = *((int *)POINTER_ADD(str, 0x18));
    char *cstr;
    if (buffersize < 16)
        cstr = POINTER_ADD(str, 0x4);
    else
        cstr = *((char **)POINTER_ADD(str, 0x4));
    return cstr;
}

#else
std::string getTemplateName(void* obj)
{
	if (!obj)
		return std::string("");

	void* templat = *(void**)((char*)(obj)+0x28);
	char* str = *(char**)((char*)(templat)+0x18);
	std::string lower = std::string(str);
	tolower(&lower);
	return lower;
}
#endif


    unsigned int getCurrentTick() {
#if WIN32
         return *(unsigned int*)0x85D090;
#else
		 return *(unsigned int*)0x10AA1F8;	 
#endif
    }
