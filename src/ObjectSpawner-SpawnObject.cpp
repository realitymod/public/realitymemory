#include "ObjectSpawner-SpawnObject.hpp"




namespace ObjectSpawnerSpawnObject
{
	std::set<std::string> templates;
	bool initialized = false;
	py::object pyCallback;
	
	void addTemplate(py::bytes name) {
		std::string lower = name;
		tolower(&lower);
		templates.insert(lower);
	}

#ifdef WIN32
	void __cdecl callback(void* obj) 
#else 
	__attribute__((optimize("O0"))) void callback()
#endif
	{
		#ifndef WIN32
		register void* objr asm("rdi"); // TODO gotta figure out how to do this properly
		void* obj = objr;
		#endif


		py::object pyObj;
		if (!obj)
			return;

		if (templates.count(getTemplateName(obj)) == 0)
			return;
		
		

		BF2PYTHON_WRAPOBJECTTO(obj, pyObj);

		py::object res = pyCallback.call(pyObj);
		
		return;
	}
	
	#ifdef WIN32
	__declspec(naked) void* hook()
	{

	// EDI free register
	__asm {
			// do original stuff
			mov     edx, [ecx];
			call    dword ptr[edx + 118h];


			push eax; // push to function 
			call callback;
			pop eax; // restore object (CDECL = caller cleanup)

			mov edi, 0x00570C7C;
			jmp edi;
		}
	}
	#else
	__attribute__((naked, noinline, optimize("O0"))) void *hook()
	{	

		// do original stuff
		asm("movss   [rsp+0xD8], xmm2"); // TODO remove IDA varnames
		asm("movss   [rsp+0xD4], xmm1");
		asm("call rbx");
		// backup result (RBX free register)
		asm("mov rbx, rax");

		asm("mov rdi, rax");
		// Call callback
		callback();
		/////////


		asm("mov rax, rbx"); // restore result
		asm("mov rbx, 0x5304BB");
		asm("jmp rbx");
	}
	#endif

	void initializeHook(py::object callback)
	{
		if (initialized)
			return;
		initialized = true;
		pyCallback = callback;
		
	#ifdef WIN32
		injectJump((void*)0x00570C74, &hook);
	#else
		injectJump((void*)0x005304A7, (void*)&hook);
	#endif
	}

}
