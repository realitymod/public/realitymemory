#ifndef OBJECTSPAWNERSPAWNOBJECT__H
#define OBJECTSPAWNERSPAWNOBJECT__H


#include <pybind11/functional.h>
#include <pybind11/pybind11.h>
#include <set>
#include <map>
#include "bf2_structs.hpp"
#include "realitymemory.hpp"
#include "utils.hpp"
#include "hook.hpp"

namespace py = pybind11;
namespace ObjectSpawnerSpawnObject
{
	void addTemplate(py::bytes name);
	#ifdef WIN32
		void __cdecl callback(void* obj);
	#else 
		__attribute__((optimize("O0"))) void callback();
	#endif
	void initializeHook(py::object callback);
	
	void* hook();
}

#endif