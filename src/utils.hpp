#ifndef BF2UTILS__H
#define BF2UTILS__H
#define POINTER_ADD(ptr, offset) (((char *)ptr) + offset)
#define POINTER_DEREF(ptr, offset) (*(void **)(((char *)ptr) + offset))

#include <cassert>
#include <pybind11/functional.h>
#include <pybind11/pybind11.h>
#include <set>
#include <map>
#include "bf2_structs.hpp"
#include "realitymemory.hpp"
#include <string>
#include <algorithm>






#if WIN32
std::string getTemplateName(void* weapon);
class STD71_STRING
{
    char data[0x1C];
};
typedef STD71_STRING *pSTD71_STRING;
const char *cstr_from_std_string(pSTD71_STRING str);

#else
std::string getTemplateName(void* weapon);
#endif

void tolower(std::string*);

unsigned int getCurrentTick();

#endif


