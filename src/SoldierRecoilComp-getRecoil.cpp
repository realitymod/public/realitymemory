
#include "diceSTL.hpp"
#include "hook.hpp"
#include "utils.hpp"

namespace GetRecoilHook
{
#define SoldierBasedRecoilComp_IWeaponComp ((void *)0x7D9268)
#define SoldierBasedRecoilComp_IObjectComp ((void *)0x7D9314)

class SoldierBasedRecoilComp
{
   public:
#ifdef WIN32
    void **IObjectCompVptr;  // 0x0
    int unknown[3];
    void **IWeaponCompVptr;         // 0x10
    int unknown2[0x13];             // 0x14
    unsigned int tickbuffer[0x0A];  // 0x60
    unsigned int tickbufferIndex;   // 0x88
#else
    void **IObjectCompVptr;         // 0x00
    int unknown[6];                 // 0x08
    void **IWeaponCompVptr;         // 0x20
    int unknown2[0x17];             // 0x28
    unsigned int tickbuffer[0x0A];  // 0x84
    unsigned int tickbufferIndex;   // 0xAC

#endif

    void *getIObjectComp() { return &this->IObjectCompVptr; }
    void *getIWeaponComp() { return &this->IWeaponCompVptr; }

    unsigned int getBufferEntry(int index)
    {
        assert(index >= 0);
        assert(index < 10);
        return this->tickbuffer[index];
    }

    void setBufferEntry(int index, unsigned int value)
    {
        assert(index >= 0);
        assert(index < 10);
        this->tickbuffer[index] = value;
    }

    /// <summary>
    /// Gets current index in the fireTick buffer
    /// </summary>
    /// <returns>-1 if not fired yet</returns>
    int getBufferIndex()
    {
        int val = this->tickbufferIndex;
        assert(val < 10);
        return val;
    }

    diceSTL::STLVector<float> *getRecoilVector()
    {
        void *ret;
#ifdef WIN32
        void *weaponcomp = this->getIWeaponComp();
        __asm {
				    mov ecx, weaponcomp;
				    mov eax, DWORD PTR[ecx];
				    call dword ptr[eax + 0xA8];
				    mov ret, eax;
        }
#else
        void **vptr =
            this->IObjectCompVptr;  // 0x178 on IObject interface, entry 0x2F
        void *(*func)(void *) = (void *(*)(void *))vptr[0x2F];
        ret = func(this);
#endif

        return (diceSTL::STLVector<float> *)ret;
    }
};

// Recoil comp at base (IObjectComp), not at 0x10 (IWeaponComp)
// __thiscall (MSVC can't compile without a this)
// recoilComp at ecx
// amount at arg
// return in float stack

#if WIN32
float __fastcall hook(SoldierBasedRecoilComp *objectComp, float amount)
#else
float hook(SoldierBasedRecoilComp *objectComp, float amount)
#endif
{
    // BF2 sometimes passes 0 (yes, real 0) we can skip everything then
    if (amount == 0) return 0;

    const int bufferStart = objectComp->getBufferIndex();
    // If we did not shoot, just return 0
    if (bufferStart < 0) return 0;

    diceSTL::STLVector<float> *recoilVector = objectComp->getRecoilVector();
    const unsigned int vectorSize = recoilVector->size();
    const unsigned int currentTick = getCurrentTick();

    const unsigned int startFireTick = objectComp->getBufferEntry(bufferStart);
    const unsigned int startTicksAgo = currentTick - startFireTick;

    if (startTicksAgo > vectorSize)
    {
        // If start tick is too old, no need to check rest
        return 0;
    }

    float sum = 0.0f;
    // Iterate whole buffer and add everything
    for (int i = 0; i < 10; i++)
    {
        const unsigned int fireTick = objectComp->getBufferEntry(i);
        const unsigned int ticksAgo = currentTick - fireTick;

        // ticksAgo might be bigger than vectorSize, so it underflows
	// However it never will underflow enough to create valid index
        const unsigned int graphIndex = vectorSize - ticksAgo;

        // Make sure index is within graph size
        // on single fire tickAgo might be 0, we skip that one
        // that means first recoil kick is 1 tick delayed
        if (graphIndex < vectorSize)
        {
            // The Graph is reversed, first frame sits at the end, last at the front
            const float frameValue = (*recoilVector)[graphIndex];
            // When goBackOnRecoil is active, only apply the positive frames for latest frame 
            sum += frameValue * (frameValue < 0 || i == bufferStart);
        }
    }
    return amount * sum;
}

void initializeHook()
{
#ifdef WIN32
    overwriteCall((void *)0x005A84FA, &hook);
    overwriteCall((void *)0x005A84EA, &hook);
#else
    injectJump((void *)0x60C9D0, (void*)&hook);
#endif
}
}  // namespace GetRecoilHook