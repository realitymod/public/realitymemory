
#include "hook.hpp"
#ifdef WIN32
bool overwriteCall(void *callPosition, void* newFunction)
{
    char *_callPosition = (char *)callPosition;
    DWORD oldProtect;
    DWORD newProtect;  // "If this parameter is NULL the function fails." thanks
                       // windows

    if (_callPosition[0] == (char)0xE8)
    {  // This is a offset call
        unsigned int offset =
            (unsigned int)(newFunction) - ((unsigned int)callPosition + 5);
        VirtualProtect(_callPosition + 1, 4, 0x40, &oldProtect);
        memcpy(_callPosition + 1, &offset, 4);
        return VirtualProtect(_callPosition + 1, 4, oldProtect, &newProtect);
    }
    //else if (*_callPosition == 0xFF)
    //{  // This is a direct call
    //    VirtualProtect(_callPosition + 1, 4, 0x40, &oldProtect);
    //    memcpy(_callPosition + 1, &newFunction, 4);
    //    VirtualProtect(_callPosition + 1, 4, oldProtect, &newProtect);
    //}
    else
    {
        return false;
    }
}

void injectJump(void* pos, void* target) {
	// mov to eax and jmp eax 
    char data[] = {0xB8, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xE0};
    memcpy(data + 1, &target, 4);
	
	DWORD oldProtect;
    DWORD newProtect; 
	VirtualProtect(pos, 7, 0x40, &oldProtect);
    memcpy(pos, data, sizeof(data));
    VirtualProtect(pos, 7, oldProtect, &newProtect);
}

void injectFarJump(void* pos, void* target)
{
    // offset jump
    char data[] = {0xE9, 0x00, 0x00, 0x00, 0x00};
    unsigned int offset = ((char *)target - (char *)pos) - 5;
    memcpy(data + 1, &offset, 4);

    DWORD oldProtect;
    DWORD newProtect;
    VirtualProtect(pos, 5, 0x40, &oldProtect);
    memcpy(pos, data, sizeof(data));
    VirtualProtect(pos, 5, oldProtect, &newProtect);
}


void overwriteVTableEntry(void *entryPos, void* newFunction)
{
    DWORD oldProtect;
    DWORD newProtect;
    VirtualProtect(entryPos, 4, 0x40, &oldProtect);
    memcpy(entryPos, &newFunction, 4);
    VirtualProtect(entryPos, 4, oldProtect, &newProtect);
}



void writeProtected(void* address, const char* bytes, int size) {
    DWORD oldProtect;
    DWORD newProtect;
    VirtualProtect(address, 7, 0x40, &oldProtect);
    memcpy(address, bytes, size);
    VirtualProtect(address, 7, oldProtect, &newProtect);
}


#else
void injectJump(void* pos, void* target) {
	unsigned char bytes[] = {
		0x48, 0xB8, // mov immediate rax
		0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // immediate
		0xff, 0xe0}; // jmp rax
	memcpy(bytes + 2, &target, 8);

	mprotect((void*)((unsigned long)pos & (unsigned long)0xFFFFFFFFFFFFF000), 0x1000, PROT_READ | PROT_WRITE | PROT_EXEC);
	memcpy(pos, bytes, 0x0C);
	mprotect((void*)((unsigned long)pos & (unsigned long)0xFFFFFFFFFFFFF000), 0x1000, PROT_READ | PROT_EXEC);
}
void injectJumpRBX(void* pos, void* target) {
    unsigned char bytes[] = {
        0x48, 0xBB, // mov immediate rbx
        0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // immediate
        0xff, 0xe3 }; // jmp rbx
    memcpy(bytes + 2, &target, 8);

    mprotect((void*)((unsigned long)pos & (unsigned long)0xFFFFFFFFFFFFF000), 0x1000, PROT_READ | PROT_WRITE | PROT_EXEC);
    memcpy(pos, bytes, 0x0C);
    mprotect((void*)((unsigned long)pos & (unsigned long)0xFFFFFFFFFFFFF000), 0x1000, PROT_READ | PROT_EXEC);
}
void overwriteVTableEntry(void* entryPos, void* newFunction)
{
    mprotect((void*)((unsigned long)entryPos & (unsigned long)0xFFFFFFFFFFFFF000), 0x1000, PROT_READ | PROT_WRITE | PROT_EXEC);
    memcpy(entryPos, &newFunction, 0x08);
    mprotect((void*)((unsigned long)entryPos & (unsigned long)0xFFFFFFFFFFFFF000), 0x1000, PROT_READ | PROT_EXEC);
}
void writeProtected(void* address, const char* bytes, int size) {
    mprotect((void*)((unsigned long)address & (unsigned long)0xFFFFFFFFFFFFF000), 0x1000, PROT_READ | PROT_WRITE | PROT_EXEC);
    memcpy(address, bytes, size);
    mprotect((void*)((unsigned long)address & (unsigned long)0xFFFFFFFFFFFFF000), 0x1000, PROT_READ | PROT_EXEC);
}

#endif


#include <stringobject.h>

void writeProtectedPy(py::int_ address, py::bytes data) {
    std::string s = data;

#ifdef WIN32
    writeProtected((void*)(unsigned int)address, s.data(), py::len(data));
#else
    writeProtected((void*)(unsigned long long)address, s.data(), py::len(data));
#endif
}