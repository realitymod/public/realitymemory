#ifndef SELECTEMPTYSEAT__H
#define SELECTEMPTYSEAT__H

#include <pybind11/functional.h>
#include <pybind11/pybind11.h>
#include <set>
#include <map>
#include "bf2_structs.hpp"
#include "realitymemory.hpp"
#include <stdio.h>
#include <string.h>
#include "hook.hpp"
#include "utils.hpp"


namespace py = pybind11;

namespace SelectEmptySeat {
	void initializeHook();
}

#endif