#ifndef PLAYERSETSPAWNGROUP__H
#define PLAYERSETSPAWNGROUP__H

#include <pybind11/functional.h>
#include <pybind11/pybind11.h>
#include "hook.hpp"
#include "utils.hpp"

namespace py = pybind11;
namespace SetSpawnGroup
{
	void initializeHook(py::function _callback);
}




#endif
