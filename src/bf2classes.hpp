namespace dice {

	class CPlayerInput;
	class CWeaponState;
	typedef int FireRate;
	class CObject;
	class Vec3;
	class Mat4;

	class CPlayer;
	class CObject;
	class CGenericFireArm;

	class CObjectTemplate {
	public:
		virtual int addRef(); //
		virtual int getRef(); //
		virtual int release(); //
		virtual void* queryInterface(unsigned int id); //
		virtual unsigned int getClassID(); //
	};

	class CObject {
	public:
		void* vptr;
		unsigned int flags;                      // 0x0004
		void* _0x0008;
		CObject* objectRoot;                     // 0x000C
		unsigned int objectId;                   // 0x0010
		CObjectTemplate* objectTemplate;         // 0x0014
#ifdef _WIN32
		char _0x0014[0x13C];
#else
		char _0x0028[0x208];
#endif
	};

	constexpr unsigned int CID_SoldierTemplate = 0x09493;

	enum class SoldierPose : int {
		Stand = 0,
		Crouch = 1,
		Prone = 2,
		Swim = 3
	};

	class CSoldier
    {
    public:
        virtual int addRef(); //
        virtual int getRef(); //
        virtual int release(); //
        virtual void* queryInterface(int id); //
        virtual bool testIfValidTransformation(SoldierPose, Mat4*, bool);
        virtual CGenericFireArm* getActiveWeapon(int);
        virtual void getActiveItemIndex();
        virtual void addItem();
#ifdef _WIN32
        virtual void addItem_();
#endif
        virtual CObject* getItem(int);
        virtual void removeItem();
        virtual void removeItem_();
        virtual void getUsedVehicle();
        virtual CPlayer* getOccupingPlayer();
        virtual SoldierPose getPose();
        // ...
    };

	struct _CSoldier {
		CObject _Object;
		void* _IPlayerControlObject; // 0x154
		void* _IKitObject; // 0x158
		CSoldier _Soldier; // 0x15C
	};

    
	class CPlayer
	{
	public:
		virtual void addRef(); // 0x0
		virtual void getRef(); // 0x4
		virtual void release(); // 0x8
		virtual void queryInterface(unsigned int); // 0xC
		virtual void getClassID(); // 0x10
		virtual void Destructor_1(); // 0x14
#ifndef _WIN32
		virtual void Destructor_2(); //
#endif
		virtual void getWeakPtr(); // 0x18
		virtual void setName(); // 0x1C
		virtual void getName(); // 0x20
		virtual void setFlags(); // 0x24
		virtual void testFlags(); // 0x28
		virtual void handleUpdate(float, unsigned int); // 0x2C
		virtual void handleFrameUpdate(float); // 0x30
		virtual void getUpdateFrequencyType(); // 0x34
		virtual void setUpdateFrequencyType(); // 0x38
		virtual void handleInput(CPlayerInput*, float, unsigned int); // 0x3C
		virtual void getAutoInput(CPlayerInput*, bool); // 0x40
		virtual CObject* getVehicle(); // 0x44
		// ...
	};

	class CGenericFireArm
	{
	public:
		virtual void addRef();
		virtual void getRef();
		virtual void release();
		virtual void* queryInterface(unsigned int);
		virtual void getTimeToNotAllowChange();
		virtual void getFireRate();
		virtual void getFireAtTick();
		virtual void isFiring();
		virtual void isReadyToUseFire();
		virtual void getHeat();
		virtual void setHeat(float);
		virtual bool getHasHeat();
		virtual void getUsesHeat();
		virtual void getShowHudIcon();
		virtual void getFireInput();
		virtual void getAltFireInput();
		virtual void launchesProjectile(int);
		virtual void getAbilityObjectList();
		virtual void getAbilityWorking();
		virtual void getAbility(void);
		virtual void setAmmoInMag(unsigned int, int);
		virtual void setFireRate(FireRate);
		virtual void setZoom(int);
		virtual void setWeaponState(unsigned int);
		virtual void setStateSync(CWeaponState const&);
		virtual CPlayer* getPlayer();
		// ...
	};

	class IAnimatedBundle {
#ifdef _WIN32
		char _IAnimatedBundle[0x18];
#else
		char _IAnimatedBundle[0x30];
#endif	
	};

	struct _CGenericFireArm
	{
		CObject _Object;
		IAnimatedBundle _IAnimatedBundle; // 0x0154 | 0x0238
		void* _IItemObject;               // 0x016C | 0x0268
		CGenericFireArm _GenericFirearm;  // 0x0170 | 0x0270
	};

	class CDefaultZoomCompTemplate {
    public:
        virtual void addRef();
        virtual void getRef();
        virtual void release();
        virtual void queryInterface();

        void* _0x0004_08;
        int   zoomInput;                   // 0x08 | 0x10
        char  onlyZoomWhenProne;           // 0x0C | 0x14
    };

    class CDefaultZoomComp {
    public:
        virtual void addRef();
        virtual void getRef();
        virtual void release();
        virtual void queryInterface();

        void* zoomTemplate;             // 0x0004 | 0x0008
        int refCount;                   // 0x0008 | 0x0010
        int zoomFactorIndex;            // 0x000C | 0x0014
        unsigned int zoomChangeTick;    // 0x0010 | 0x0018
        char zoomLodFlag;               // 0x0014 | 0x001C
        char _0x0015_1D;
        char fovFlag;                   // 0x0016 | 0x001E
        char _0x0017;                   // 0x0017 | 0x001F
        char _0x0018[4];
        char unkFlag;                   // 0x001C | 0x0024
        char _0x0025[3];
        CDefaultZoomCompTemplate* temp; // 0x0020 | 0x0028
	};

	    class _CDefaultZoomComp {
    public:
        virtual void addRef();
        virtual void getRef();
        virtual void release();
        virtual void queryInterface();

        int refCount;                      // 0x0004 | 0x0008
#ifndef _WIN32
        char _0x0C[0x4];
#endif
        void* componentInfo;               // 0x0008 | 0x0010
        _CGenericFireArm* weaponObject;    // 0x000C | 0x0018
        CDefaultZoomComp _DefaultZoomComp;
	};
}