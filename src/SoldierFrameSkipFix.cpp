#include "SoldierFrameSkipFix.h"
namespace SoldierFrameSkipFix
{

#ifdef _WIN32
#ifdef W32DED
#define LOOK_SIDE_RESTORE 0x8858E8
#define SOLDIER_YAW_INPUT_VAL (ebp - 0x24)
#define CAMERA_YAW_INPUT_VAL (ebp + 0x08)
#define HANDLE_PLY_INPUT_HOOK_1 0x5516B7
#define HANDLE_PLY_INPUT_HOOK_2 0x5516D8
#else
#define LOOK_SIDE_RESTORE 0x9EC2CC
#define SOLDIER_YAW_INPUT_VAL (ebp - 0x2C)
#define CAMERA_YAW_INPUT_VAL (ebp - 0x04)
#define HANDLE_PLY_INPUT_HOOK_1 0x5AE805
#define HANDLE_PLY_INPUT_HOOK_2 0x5AE826
#endif  // W32DED

void __declspec(naked) fixDoubleYawPositive()
{
    __asm {
        fstp    dword ptr[ebx + 0x248]
        fld     dword ptr [SOLDIER_YAW_INPUT_VAL]
        mov     eax, LOOK_SIDE_RESTORE
        mov     eax, [eax]
        fcomp   dword ptr [eax]
        fnstsw  ax
        test    ah, 0x45
        je      adjustCameraYaw  // if delta > lookSideRestore
            // move only the camera by delta, lower body no change
        mov     dword ptr[SOLDIER_YAW_INPUT_VAL], 0
        jmp     hookExit
    adjustCameraYaw:
            // move only the lower body by delta, camera no change
        fld     dword ptr[ebx + 0x240]
        fsub    dword ptr[CAMERA_YAW_INPUT_VAL]
        fstp    dword ptr[ebx + 0x240]
    hookExit:
        mov     eax, (HANDLE_PLY_INPUT_HOOK_1 + 6)
        jmp     eax
    }
}

void __declspec(naked) fixDoubleYawNegative()
{
    __asm {
        fstp    dword ptr[ebx + 0x248]
        fld     dword ptr[SOLDIER_YAW_INPUT_VAL]
        fchs
        mov     eax, LOOK_SIDE_RESTORE
        mov     eax, [eax]
        fcomp   dword ptr [eax]
        fnstsw  ax
        test    ah, 0x45 
        je      adjustCameraYaw  // if -delta > lookSideRestore
        mov     dword ptr[SOLDIER_YAW_INPUT_VAL], 0
        jmp     hookExit
    adjustCameraYaw:
        fld     dword ptr [ebx + 0x240]
        fsub    dword ptr [CAMERA_YAW_INPUT_VAL]
        fstp    dword ptr [ebx + 0x240]
    hookExit:
        mov     eax, (HANDLE_PLY_INPUT_HOOK_2 + 6)
        jmp     eax
    }
}
#else  // _WIN32
__attribute__((naked)) void fixDoubleYawPositive()
{
    __asm__ volatile(
        "movss   dword ptr [rbp+0x3E0], xmm1\n\t"
        "mov     rax, cs:0x108EA90\n\t"  // lookSideRestore
        "movss   xmm3, dword ptr [rsp+0x1D4]\n\t"
        "ucomiss xmm3, dword ptr [rax]\n\t"
        "ja      adjustCameraYawP\n\t"          // if delta > lookSideRestore
        "mov     dword ptr [rsp+0x18C], 0\n\t"  // SOLDIER_YAW_INPUT_VAL
        "xorps   xmm6, xmm6\n\t"
        "mov     rax, 0x54F8DC\n\t"
        "jmp     rax\n\t"
        "adjustCameraYawP:\n\t"
        "movss   xmm3, dword ptr [rbp+0x3D8]\n\t"
        "subss   xmm3, dword ptr [rsp+0x1D4]\n\t"  // CAMERA_YAW_INPUT_VAL
        "movss   dword ptr [rbp+0x3D8], xmm3\n\t"
        "xorps   xmm6, xmm6\n\t"
        "movss   xmm3, dword ptr [rsp+0x1D4]\n\t"
        "movss   [rsp+0x18C], xmm3\n\t"
        "mov     rax, 0x54F8DC\n\t"
        "jmp     rax\n\t");
}

__attribute__((naked)) void fixDoubleYawNegative()
{
    __asm__ volatile(
        "movss   dword ptr [rbp+0x3E0], xmm1\n\t"
        "movss   xmm3, dword ptr [rsp+0x1D4]\n\t"
        "xorps   xmm3, cs:0xB2F430\n\t"
        "mov     rax, cs:0x108EA90\n\t"  // lookSideRestore
        "ucomiss xmm3, dword ptr [rax]\n\t"
        "ja      adjustCameraYawN\n\t"          // if -delta > lookSideRestore
        "mov     dword ptr [rsp+0x18C], 0\n\t"  // SOLDIER_YAW_INPUT_VAL
        "xorps   xmm6, xmm6\n\t"
        "mov     rax, 0x54F8DC\n\t"
        "jmp     rax\n\t"
        "adjustCameraYawN:\n\t"
        "movss   xmm1, dword ptr [rbp+0x3D8]\n\t"
        "subss   xmm1, dword ptr [rsp+0x1D4]\n\t"  // CAMERA_YAW_INPUT_VAL
        "movss   dword ptr [rbp+0x3D8], xmm1\n\t"
        "xorps   xmm6, xmm6\n\t"
        "movss   xmm1, dword ptr [rsp+0x1D4]\n\t"
        "movss   [rsp+0x18C], xmm1\n\t"
        "mov     rax, 0x54F8DC\n\t"
        "jmp     rax\n\t");
}
#endif

void initializeHook()
{
#ifdef _WIN32
    injectFarJump((void *)HANDLE_PLY_INPUT_HOOK_1, fixDoubleYawPositive);
    injectFarJump((void *)HANDLE_PLY_INPUT_HOOK_2, fixDoubleYawNegative);
#else
    injectJump((void *)0x5506C5, (void*)&fixDoubleYawPositive);
    injectJump((void *)0x550754, (void*)&fixDoubleYawNegative);
#endif
}

}  // namespace SoldierFrameSkipFix