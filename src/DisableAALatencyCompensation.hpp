#ifndef AALAGCOMP__H
#define AALAGCOMP__H

#include <pybind11/functional.h>
#include <pybind11/pybind11.h>
#include "hook.hpp"
#include "utils.hpp"

namespace py = pybind11;
namespace AALagComp
{
	void addTemplate(py::bytes name);
	void initializeHook();
}




#endif
