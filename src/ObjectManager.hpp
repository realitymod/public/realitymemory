#ifndef OBJECTMANAGER_H
#define OBJECTMANAGER_H
#include <pybind11/functional.h>
#include <pybind11/pybind11.h>

namespace py = pybind11;

namespace ObjectManager
{

// Message for setting time to live
#define MESSAGE_SET_TTL 0x1D

    /// <summary>
    /// Posts a message
    /// </summary>
    /// <param name="object"></param>
    /// <param name="message"></param>
    /// <param name="time"></param>
    void postMessage(void* object, int message, float time);

    void postMessage_py(py::int_ bf2objpointer, py::int_ message, py::float_ time);
}

#endif