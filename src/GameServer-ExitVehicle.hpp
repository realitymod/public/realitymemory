#ifndef EXITVEHICLE_H
#define EXITVEHICLE_H

#include <pybind11/functional.h>
#include <pybind11/pybind11.h>
#include "realitymemory.hpp"


namespace py = pybind11;

namespace GameServerExitVehicle
{
	void ExitVehicle(py::int_ iplayer, py::bool_ b1, py::bool_ b2);

};

#endif