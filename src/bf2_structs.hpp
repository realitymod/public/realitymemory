#ifndef REALITYMEMORYSTRUCTS_H
#define REALITYMEMORYSTRUCTS_H

#include <stdint.h>

#ifdef WIN32


#define TICK_COUNT (0x085D090) // int32_t
#define WORLD_TIME (0x085D088) // Double

#define ObjPhysics_staticVPTR (0x007FA520)
#define ObjPhysics_soldierVPTR (0x007F9CA0)
#define ObjPhysics_normalPhysicsVPTR (0x007FA700)
#define ObjPhysics_pointVPTR (0x007FABF8)
#define dice__hfe__serverSettings (0x00841620)
#define squadManagerPtrPtr (0x0083BF54)
#define playerManagerPtrPtr (0x0089AE50)

#else // Linux

#define TICK_COUNT (0x10AA1F8) // int32_t
#define WORLD_TIME (0x10AA200)

#define ObjPhysics_staticVPTR (0xB95E10)
#define ObjPhysics_soldierVPTR (0x0B95490)
#define ObjPhysics_normalPhysicsVPTR (0xB93EB0)
#define ObjPhysics_pointVPTR (0xB944B0)
#define squadManagerPtrPtr (0x010b8c00)
#define dice__hfe__serverSettings (0x108b660)
#define playerManagerPtrPtr (0x112EE38)
#endif // WIN32

// Set alignment to 1 for all structs
#pragma pack(push, 1)

struct SprintState
{
    float dissipation_time;  // 0x0
    float recovery_time;     // 0x4
    float minimum_to_sprint;  // 0x8
    float unknown;          // 0xc
    float stamina;          // 0x10
};

struct SharedPtr
{
    int32_t refcount;   // 0x0
    int8_t padding[4];  // 0x4
    void *ptr;          // 0x8
};

struct RagdollParticle
{
    float x;  // 0x0
    float y;  // 0x4
    float z;  // 0x8

    int8_t unknown[24];  // 0xc

    float vx;  // 0x24
    float vy;  // 0x28
    float vz;  // 0x2c

    int8_t unknown1[40];  // 0x30
};

struct Ragdoll
{
    int8_t unknown[8];  // 0x0
    // &std::Vector<Ragdoll::Particle>, treat as pointer to array and just
    // select first, looks like first index is body VECTOR LENGTH = 13 ENTRY
    // SIZE = 0x58
    RagdollParticle *particles[13];  // 0x8
#ifdef WIN32
    int8_t unknown1[17];              // 0x3c
    bool is_awake;                    // 0x4d
    int8_t unknown2[26];              // 0x4e
    float sleep_timer;                // 0x68
#else // Linux
    int8_t unknown1[33];             // 0x70
    bool is_awake;                    // 0x91
    int8_t unknown2[26];             // 0x92
    float time_to_sleep;               // 0xac
#endif // WIN32
};

struct ObjPhysicsSoldier
{
    void *vptr;             // 0x0
#ifdef WIN32
    int8_t unknown[28];     // 0x4
    float vx;               // 0x20
    float vy;               // 0x24
    float vz;               // 0x28
    int8_t unknown2[56];    // 0x2c
    int32_t sleepiness_max;  // 0x64
    int32_t sleepiness;     // 0x68
    int8_t unknown3[64];    // 0x6c
    int32_t on_ground;       // 0xac
#else // Linux
    int8_t unknown[36];     // 0x8
    float vx;               // 0x2c
    float vy;               // 0x30
    float vz;               // 0x34
    int8_t unknown2[56];    // 0x38
    int32_t sleepiness_max;  // 0x70
    int32_t sleepiness;     // 0x74
    // TODO random guess based on some code that looks alike but offsets do
    // align (0x40 from sleepiness to onGround)
    int8_t unknown3[64];    // 0x78
    int32_t on_ground;       // 0xb8
#endif // WIN32
};

struct ObjPhysicsNormal
{
    void *vptr;             // 0x0
#ifdef WIN32
    int8_t unknown[28];     // 0x4
    float vx;               // 0x20
    float vy;               // 0x24
    float vz;               // 0x28
    int8_t unknown2[56];    // 0x2c
    int32_t sleepiness_max;  // 0x64
    int32_t sleepiness;     // 0x68
#else // Linux
    int8_t unknown[36];     // 0x8
    float vx;               // 0x2c
    float vy;               // 0x30
    float vz;               // 0x34
    int8_t unknown1[56];    // 0x38
    int32_t sleepiness_max;  // 0x70
    int32_t sleepiness;     // 0x74
#endif // WIN32
};

struct ObjPhysicsPoint
{
    void *vptr;             // 0x0
#ifdef WIN32
    int8_t unknown[28];     // 0x4
    float vx;               // 0x20
    float vy;               // 0x24
    float vz;               // 0x28
    int8_t unknown2[56];    // 0x2c
    int32_t sleepiness_max;  // 0x64
    int32_t sleepiness;     // 0x68
#else // Linux
    int8_t unknown[36];     // 0x8
    float vx;               // 0x2c
    float vy;               // 0x30
    float vz;               // 0x34
    int8_t unknown1[56];    // 0x38
    int32_t sleepiness_max;  // 0x70
    int32_t sleepiness;     // 0x74
#endif // WIN32
};

struct ArmorComponent {
    int8_t unknown1[28];                 // 0x0
    int32_t lastHitMaterialIndex;       // 0x1c
    int32_t lastGroundHitMaterialIndex; // 0x20
    int8_t unknown2[12];                 // 0x24
    float lastHitx;                     // 0x30
    float lastHity;                     // 0x34
    float lastHitz;                     // 0x38
};

struct CObject
{
#ifdef WIN32
    void *vptr;                     // 0x0
    int32_t object_flags;           // 0x4
    void *unknown1;                 // 0x8
    void *obj_root;                 // 0xc
    int32_t object_id;              // 0x10
    void *obj_template;             // 0x14
    int32_t unknown2;               // 0x18
    void *unknownptr1;              // 0x1c
    void *unknownptr2;              // 0x20
    int32_t unknown3;               // 0x24
    void *object_hierarchy_root;    // 0x28
    void *object_hierarchy_left;    // 0x2c
    void *object_hierarchy_right;   // 0x30
    int8_t unknown4[4];             // 0x34
    void *object_mesh;              // 0x38
    ArmorComponent *armorComponent; // 0x3c
    void *object_collision;         // 0x40
    void *obj_physics;              // 0x44
    void *unknownptr3;              // 0x48
    void *unknownptr4;              // 0x4c
    int8_t unknown5[56];            // 0x50
    float matrix[16];               // 0x88
    float matrix_transpose[16];     // 0xc8
    // ("unknown6", c_int8 * 28),
    // ("boundingSphereRadius", c_float),
    // ("unknown7", c_int8 * 28),
    char object_name[28];           // 0x108
    int8_t unknown8[40];            // 0x418
    int32_t object_end;             // 0x440
#else // Linux
    // TODO: should work, test it
    int8_t unknown[24];    // 0x0
    void *obj_root;        // 0x18
    int32_t object_id;     // 0x20
    int8_t unknown1[100];  // 0x24
    void *obj_physics;      // 0x88
#endif // WIN32

};


struct CSoldier
{
#ifdef WIN32
    CObject c_object;         // 0x0
    int8_t unknown1 [224];    // 0x150
    int32_t pose;             // 0x230
    int8_t unknown2 [32];     // 0x234
    Ragdoll *ragdoll;         // 0x254
    int8_t unknown3[220];     // 0x258
    SprintState sprint_state; // 0x334
#else // Linux
    int8_t unknown1[968];     // 0x0
    int32_t pose;             // 0x3c8
    int8_t unknown2[24];      // 0x3cc
    Ragdoll *ragdoll;         // 0x3f0
    int8_t unknown3[264];     // 0x3f8
    SprintState sprint_state; // 0x500
#endif // WIN32
};

struct CEngine
{
#ifdef WIN32
    CObject c_object;     // 0x0
    int8_t unknown[32];  // 0x150
    float inputx;        // 0x170
    float inputy;        // 0x174
    float inputz;        // 0x178
#else // Linux
    int8_t unknown[600];  // 0x0
    float inputx;         // 0x258
    float inputy;         // 0x25c
    float inputz;         // 0x260
#endif // WIN32
};

struct CGenericProjectile
{
#ifdef WIN32
    CObject c_object;  // 0x0
    int8_t unknown[0x60]; // 0x150
    float spawnTime; // 0x1B0
#else // Linux
    int8_t unknown[0x2A8];       // 0x0
    SharedPtr *firing_weapon;   // 0x2a8
    SharedPtr *firing_player;   // 0x2b0
    SharedPtr *firing_vehicle;  // 0x2b8
    int32_t team;              // 0x2c0
    int8_t unknown2[0x10]; // 0x2c4
    float spawnTime; // 0x2d4
#endif // WIN32
};

struct CSupply
{
#ifdef WIN32
    CObject c_object;      // 0x0
    int8_t unknown1[20];  // 0x150
    float sharedammo;     // 0x164
#else // Linux
    int8_t unknown1[588];  // 0x0
    float sharedammo;      // 0x24c
#endif // WIN32
};

struct CSpawnPoint
{
#ifdef WIN32
    CObject c_object;     // 0x0
    int8_t unknown[4];   // 0x150
    int32_t spawn_group;  // 0x154
#else // Linux
    int8_t unknown[564];  // 0x0
    int32_t spawn_group;   // 0x234
#endif // WIN32
};

struct TargetingComponent
{
#ifdef WIN32
    int8_t unknown1[72];  // 0x0
    void *target_object;   // 0x48
#else // Linux
    int8_t unknown1[112];  // 0x0
    void *target_object;    // 0x70
#endif // WIN32
};

struct AmmoComponentTemplate
{
#ifdef WIN32
    void *vptr1;                            // 0x0
    void *vptr2;                            // 0x4
    int32_t ammo_type;                       // 0x8

    float reload_time;                       // 0xc
    int32_t reload_time_something;            // 0x10

    float extra_reload_time_first;             // 0x14
    int32_t extra_reload_time_first_something;  // 0x18
    float extra_reload_time_last;              // 0x1c
    int32_t extra_reload_time_last_something;   // 0x20

    float change_mag_at;                      // 0x24
    float auto_reload;                       // 0x28

    float min_timeto_reload;                  // 0x2c
    int32_t min_timeto_reload_something;       // 0x30
    int32_t max_nr_of_mags;                    // 0x34
    int32_t mag_size;                        // 0x38
#else // Linux
    void *vptr1;       // 0x0
    void *vptr2;       // 0x8
    int32_t ammo_type;  // 0x10

    float reload_time;             // 0x14
    int32_t reload_time_something;  // 0x18

    float extra_reload_time_first;             // 0x1c
    int32_t extra_reload_time_first_something;  // 0x20
    float extra_reload_time_last;              // 0x24
    int32_t extra_reload_time_last_something;   // 0x28

    float change_mag_at;  // 0x2c
    float auto_reload;   // 0x30

    float min_timeto_reload;             // 0x34
    int32_t min_timeto_reload_something;  // 0x38
    int32_t max_nr_of_mags;               // 0x3c
    int32_t mag_size;                   // 0x40

    int32_t unknown2;            // 0x44
    int8_t unknown3;             // 0x48
    int8_t unknown4;             // 0x49
    int8_t padding_probably[6];  // 0x4a
    int32_t unknown5;            // 0x50
#endif // WIN32
};

struct AmmoComponent
{
#ifdef WIN32
    void *vptr;                 // 0x0
    int8_t unknown1[4];         // 0x4
    void *mag_array_start;        // 0x8
    void *mag_array_end;          // 0xc
    void *mag_array_something;    // 0x10
    int32_t someflags;          // 0x14
    int32_t last_tick_firing;     // 0x18
    int32_t unknown_tick1;       // 0x1c
    int32_t unknown_tick2;       // 0x20
    int32_t last_tick_something;  // 0x24
    int8_t unknown3[4];         // 0x28
    int32_t last_rearm_tick;      // 0x2c
    float partial_rearm;         // 0x30
    int8_t unknown4[8];         // 0x34
    int8_t unknown5[4];         // 0x3c

    int8_t unknown6[1];  // 0x40
    int8_t is_empty[1];   // 0x41
    int8_t unknown7[2];  // 0x42

    int8_t unknown8[8];                            // 0x44
    AmmoComponentTemplate *ammo_component_template;  // 0x4c
#else // Linux
    void *vptr;                   // 0x0
    int8_t unknown1[4];           // 0x8
    void *mag_array_start;          // 0xc
    void *mag_array_end;            // 0x14
    int8_t unknown2[72];          // 0x1c
    void *ammo_component_template;  // 0x64
#endif // WIN32
};

struct DeviationComponent
{
#ifdef WIN32
    void *vptr;                            // 0x0
    float deviation_actual;                 // 0x4
    float deviation_penalty_fire;            // 0x8
    int8_t unknown1[20];                   // 0xc
    float deviation_penalty_rotate;          // 0x20
    float deviation_penalty_run;             // 0x24
    float deviation_penalty_prone;           // 0x28
    float deviation_penalty_multiplier_pose;  // 0x2c
#else // Linux
    int8_t unknown1[40];         // 0x0
    float deviation_actual;       // 0x28
    float deviation_penalty_fire;  // 0x2c
#endif // WIN32
};

struct ZoomComponent
{
#ifdef WIN32
    int8_t unknown[12];   // 0x0
    int32_t current_zoom;  // 0xc
#else // Linux
    int8_t unknown[52];   // 0x0
    int32_t current_zoom;  // 0x34
#endif // WIN32
};

struct FireComponent
{
#ifdef WIN32
    int8_t unknown[28];        // 0x0
    int8_t last_fired_tick[28];  // 0x1c
#else // Linux
    int8_t unknown[152];    // 0x0
    int32_t last_fired_tick;  // 0x98
#endif // WIN32
};

struct CWeapon
{
#ifdef WIN32
    CObject c_object;                         // 0x0
    int8_t unknown1[52];                     // 0x150
    FireComponent *fire_component;            // 0x184
    ZoomComponent *unknown;                  // 0x188
    ZoomComponent *zoom_component;            // 0x18c
    void *sound_component;                    // 0x190
    AmmoComponent *ammo_component;            // 0x194
    void *recoil_component;                   // 0x198
    TargetingComponent *targeting_component;  // 0x19C
    DeviationComponent *deviation_component;  // 0x1A0
    int8_t unknown2[20];                     // 0x1A4
    float heat_prediction;                    // 0x2F8
    float heat_current;                       // 0x2FC
    int32_t heat_last_set_tick;                 // 0x300
    //
    // ("deviation", c_void_p),    // 1A0
    // ("somethingammo2readonly", c_void_p),    // 1A4
    // ("unknown3", c_int8 * 0x10),    // 1A8
    //
    // ("somethingoverheat1", c_float),    // 1B8
    // ("somethingoverheat2", c_float),    // 1BC
    //
    // ("unknown4", c_int8 * 0x50),    // 1C0
    // ("MainWeaponTicksFiredAgo", c_int32),    // 210
    // ("SecondaryWeaponTicksFiredAgo", c_int32),    // 214
#else // Linux
    int8_t unknown1[656];                    // 0x0
    FireComponent *fire_component;            // 0x290
    int8_t unknown2[8];                      // 0x298
    ZoomComponent *zoom_component;            // 0x2a0
    void *sound_component;                    // 0x2a8
    void *ammo_component;                     // 0x2b0
    void *recoil_component;                   // 0x2b8
    TargetingComponent *targeting_component;  // 0x2c0
    DeviationComponent *deviation_component;  // 0x2c8
    int8_t unknown3[40];                     // 0x2d0
    float heat_prediction;                    // 0x2f8
    float heat_current;                       // 0x2fc
    // ("heatLastSetTick", c_int32),
#endif // WIN32
};

struct CKit
{
#ifdef WIN32
    CObject c_object;         // 0x0
    int8_t unknown1[32];     // 0x150
    void *weapon_list;        // 0x170
    int32_t weapon_list_size;  // TODO: Not confirmed
#else // Linux
    CObject object;  // 0x0
#endif // WIN32
};

struct RbTreeNode
{
#ifdef WIN32
    void *left;    // 0x0
    void *parent;  // 0x4
    void *right;   // 0x8
    void *data;    // 0xc
#else // Linux
    int8_t color[8];  // 0x0
    void *parent;     // 0x8
    void *left;       // 0x10
    void *right;      // 0x18
    void *data;       // 0x20
#endif // WIN32
};

struct RbTreeBase
{
#ifdef WIN32
    void *leftest;   // 0x0
    void *root;      // 0x4
    void *rightest;  // 0x8
#else // Linux
    void *comparator;  // 0x0
    int8_t color[8];   // 0x8
    void *root;        // 0x10
    void *left;        // 0x18
    void *right;       // 0x20
    int64_t count;     // 0x28
#endif // WIN32
};

struct Order
{
    int32_t order_type;  // 0x0
    int32_t unknown1;   // 0x4
    float x;            // 0x8
    float y;            // 0xc
    float z;            // 0x10
};

struct SquadManagerSquad
{
#ifdef WIN32
    void *vptr;              // 0x0
    int8_t unknown[4];       // 0x4
    char name[16];           // 0x8
    int32_t name_size;        // 0x18
    int32_t name_max;         // 0x1c
    int32_t squad_number;     // 0x20
    int32_t current_sl;       // 0x24
    bool is_locked;           // 0x28
    bool has_squad_orders;     // 0x29
    bool has_commander_order;  // 0x2A
    // Probably 'has request order'
    bool unknown2;           // 0x2B
    int8_t unknown3[4];      // 0x2C
    Order order;             // 0x30
    int8_t unknown4[284];    // 0x44
    int32_t playercount;     // 0x160
    // TODO: offsets are off from here because I am unsure of order size
#else // Linux
    int8_t unknown1[8];             // 0x0
    void *name_ptr;                  // 0x8
    int32_t squad_id;                // 0x10
    int32_t current_sl;              // 0x14
    bool is_locked;                  // 0x18
    bool has_squad_orders;            // 0x19
    bool has_commander_orders;        // 0x1a
    bool has_id_krequestor_something;  // 0x1b
    int8_t unknown2[4];             // 0x1c
    Order order;                    // 0x20
    int8_t unknown3[220];           // 0x34
    Order commander_orders;          // 0x110
    int8_t unknown4[28];            // 0x124
    int32_t spawngroupid;           // 0x140
    int8_t unknown5[52];            // 0x144
    int32_t playercount;            // 0x178
#endif // WIN32
};

struct SquadManagerTeam
{
#ifdef WIN32
    int8_t unknown[8];                // 0x0
    void *squads;                     // 0x8
    int32_t squadcount;               // 0xc
    int8_t unknown1[4];               // 0x10
    void *squads_linked_list;           // 0x14
    int32_t squads_linked_list_count;    // 0x18
    void *vptr;                       // 0x1c
    int32_t commander;                // 0x20
    int8_t unknown2[12];              // 0x24
    int32_t commander_applicant_count;  // 0x30
    void *commander_applicants;        // 0x34
#else // Linux
    int8_t unknown[8];       // 0x0
    RbTreeBase squads_tree;  // 0x8
#endif // WIN32
};

struct SquadManager
{
#ifdef WIN32
    void *vptr;         // 0x0
    int8_t unknown[8];  // 0x4
    void *teams;        // 0xc
    int32_t teamcount;  // 0x10
    float timeouts[3];  // 0x14
#else // Linux
    void *vptr;             // 0x0
    int8_t unknown[8];      // 0x8
    RbTreeBase teams_tree;  // 0x10
    int32_t teamcount;      // 0x38
#endif // WIN32
};

struct ServerSettings
{
#ifdef WIN32
    int8_t vptr[612];       // 0x0
    int32_t reserved_slots;  // 0x264
#else // Linux
    int8_t vptr[332];       // 0x0
    int32_t reserved_slots;  // 0x14c
#endif // WIN32
};

struct PlayerManager
{
#ifdef WIN32
    int8_t unknown[76];  // 0x0
    void *players;       // 0x4c
#else // Linux
    int8_t unknown[144];  // 0x0
    RbTreeBase players;   // 0x90
#endif // WIN32
};

struct ActionBufferNode
{
#ifdef WIN32
    void *prev;         // 0x0
    void *next;         // 0x4
    int32_t tick;       // 0x8
    int8_t inputs[20];  // 0xc
#else // Linux
    void *prev;         // 0x0
    void *next;         // 0x8
    int32_t tick;       // 0x10
    int8_t inputs[20];  // 0x14
#endif // WIN32
};

struct ActionBuffer
{
#ifdef WIN32
    void *somevptridk;  // 0x0
    void *list_root;     // 0x4
    int32_t list_size;   // 0x8
#else // Linux
    void *next;  // 0x0
    void *prev;  // 0x8
#endif // WIN32
};

struct Player
{
#ifdef WIN32
    int8_t unknown[380];  // 0x0
    void *action_buffer;   // 0x17c
    int8_t unknown1[512];  // 0x180

    float side_button;     // 0x380
    int8_t unknown2[8];    // 0x384
    float forward_button;  // 0x38c
    int8_t unknown3[20];   // 0x390
    float jump_button;     // 0x3a4
    float enter_button;    // 0x3a8
    int8_t unknown4[8];    // 0x3ac
    float sprint_button;   // 0x3b4
    int8_t unknown5[96];   // 0x3b8
    float crouch_button;   // 0x418
    // crouch 418
    // unserialized inputs, useless (writing doesn't do anything):
    // ("unknown3",  c_int8 * 0x2C),    // 3AC
    // ("parachute", c_float),    // 3D8
    // ("leftclicking", c_bool),    //0x184
    // ("rightclicking", c_bool),    // 0x185
    // 0x3A8 E button
    // 0x3D8 parachute button
    // 0x414 prone button
    // 0x418 crouch button
#else // Linux
    int8_t unknown[464];  // 0x0
    void *action_buffer;   // 0x1d0

    int8_t unknown1[672];  // 0x1d8

    float side_button;     // 0x478
    int8_t unknown2[8];   // 0x47c
    float forward_button;  // 0x484
    int8_t unknown3[20];  // 0x488
    float jump_button;     // 0x49c
    float enter_button;    // 0x4a0
    int8_t unknown4[8];   // 0x4a4
    float sprint_button;   // 0x4ac
    int8_t unknown5[96];  // 0x4b0
    float crouch_button;   // 0x510
#endif // WIN32
};

#pragma pack(pop)

#endif // REALITYMEMORYSTRUCTS_H
