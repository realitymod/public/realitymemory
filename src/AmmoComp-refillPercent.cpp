#include "AmmoComp-refillPercent.hpp"




namespace AmmoCompRefillPerecnt
{
	bool initialized = false;

#ifdef WIN32	
	// args in stack
	// this in ecx
	// return in float stack
	__declspec(naked) void hook(float given, int isCalcAmmoStatus) noexcept
	{
		int ammoType;
		float partial;
		float left;
		void* _this;
		unsigned int* lastRearmTick;
		_asm {
			push ebp;
			mov ebp, esp;
			sub esp, __LOCAL_SIZE;

			// get AmmoType
			mov    eax, DWORD PTR[ecx + 0x4c];
			mov    eax, DWORD PTR[eax + 0x8];
			mov    ammoType, eax;

			// Get partial rearmed ammo
			mov    eax, DWORD PTR[ecx + 0x30];
			mov    partial, eax;

			// Make sure compiler optimization doesn't lose "this"
			mov _this, ecx;
		}

		left = given;
		if (ammoType != -1)
		{
			// Keeping partial rearmed ammo above 0.5
			// Todo that we set lastRearmTick really high so original function does not reset it
			if (partial >= 0.5f) {
				lastRearmTick = (unsigned int*) (((char*)_this) + 0x2C);
				*lastRearmTick = 0xFFFFF000;
			}

			const float modifier = ammoType > 1 ? 100.0f / ammoType : 1.0f;

                        if (!isCalcAmmoStatus)
                        {
                                // Scale how much is given
                                given *= modifier;
                        }

			_asm {
				push isCalcAmmoStatus; // no clue what this does just forward it and pretend its not here
				push given;
				mov ecx, _this; // restore ecx
				mov eax, 0x005C2780 // Call original
				call eax; 
				fstp left; // pop return from float stack
			}

			// Scale how much is actually taken
                        if (!isCalcAmmoStatus)
                        {
                                left /= modifier;
                        }
		}

		_asm {
			fld left; // Return via float stack
			mov esp, ebp;
			pop ebp;
			ret 8;  // pop args
		}
	}
#else
	float (*refillPercent)(void*, float, bool) = (float (*)(void*, float, bool))0x005EB2C0;

	__attribute__((optimize("O0"), noinline)) float hook(void* _this, float given, bool isCalcAmmoStatus) {

		void* templat = *(void**)(((char*)_this) + 0x80);
		int ammoType = *(int*)(((char*)templat) + 0x10);
		float partial = *(float*)(((char*)_this) + 0x5C);

		float left = given;
		if (ammoType != -1)
		{
			if (partial >= 0.5f) {
				*(unsigned int*)(((char*)_this) + 0x58) = 0xFFFFF000;
			}

			const float modifier = ammoType > 1 ? 100.0f / ammoType : 1.0f;

			// BF2 calls this function with excaclty 100 if it does
                        // PlayerControlObject::calcAmmoStatus In that case we
                        // dont modify it
                        if (!isCalcAmmoStatus)
                        {
                                // Scale how much is given
                                given *= modifier;
                        }

			left = refillPercent(_this, given, isCalcAmmoStatus);
			
			// Scale how much is actually taken
                        if (!isCalcAmmoStatus)
                        {
                                left /= modifier;
                        }
		}

		return left;
	}

	__attribute__((optimize("O0"), noinline)) float hookthunk(void* _this, float given, bool isCalcAmmoStatus) {
		_this = (void*)((char*)_this + 0x0FFFFFFFFFFFFFFE0);
		hook(_this, given, isCalcAmmoStatus);
}
#endif

	void initializeHook()
	{
		if (initialized)
			return;
		initialized = true;
		
		//printConsole("ammoComp init");
#ifdef WIN32
		overwriteVTableEntry((void*)0x007D85E4, &hook);
		overwriteVTableEntry((void*)0x007DE06C, &hook);
#else
		overwriteVTableEntry((void*)0x00B6FB78, ((void*)&hook));
		overwriteVTableEntry((void*)0x00B70798, ((void*)&hook));

		overwriteVTableEntry((void*)0x00B6FD58, ((void*)&hookthunk));
		overwriteVTableEntry((void*)0x00B70978, ((void*)&hookthunk));
#endif
	}

}
