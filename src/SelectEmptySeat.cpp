#include "SelectEmptySeat.hpp"


namespace SelectEmptySeat {
	py::function getFirstSeat;

	// 0-based return
#ifdef WIN32
	int __fastcall callback(int playerid) {
#else
	int callback(int playerid) {
#endif
		py::object seatIndex = getFirstSeat.call(playerid);
		return seatIndex.cast<int>();
	}


#ifdef WIN32
	__declspec(naked) void hook_windows() {
		__asm {
			mov eax, ecx;
			add eax, 0xFFFFFFFF;

			cmp ecx, 8;
			jnz skip;
			mov ecx, [ebx + 0xB8]; // move player ID to ecx
			call callback;
		skip:
			mov[ebp + 0x08], eax

			// do original
			mov     edi, [ebp + 0x0C];
			mov     ecx, edi;
			mov eax, 0x00458E30; // jump back
			jmp eax;
		}
	}
#else	
	__attribute__((naked, noinline, optimize("O0"))) void hook_linux()
	{
		register int playerid asm("rdi");


		// is it 7? (different than windows which starts 1-based)
		asm("cmp eax, 7");
		// no, skip callback
		asm("jnz __SelectEmptySeatContinue");

		// rbp is player, get his id
		asm("mov edi, [rbp + 0xD4]");

		//  callback
		callback(playerid); // returns to eax

		asm("__SelectEmptySeatContinue:");
		// store selected seat in stack in expected position
		asm("mov [rsp+0x508], eax");

		// do original stuff
		asm("mov rdi, r13 ");
		asm("mov rbx, 0x69A1E0"); // GetRootParent
		asm("call rbx");
		asm("mov rbx, 0x4ACAC4"); // continue original function
		asm("jmp rbx");
}
#endif





	void initializeHook() {
		py::module rvehicles = py::module::import("game.realityvehicles");
		getFirstSeat = (py::function)rvehicles.attr("getFirstEmptySeat");


#ifdef WIN32

		// mov ecx, eax
		char data[] = { 0x89, 0xc1 };
		DWORD oldProtect;
		DWORD newProtect;
		VirtualProtect((LPVOID)0x00458E25, sizeof(data), 0x40, &oldProtect);
		memcpy((LPVOID)0x00458E25, data, sizeof(data));
		VirtualProtect((LPVOID)0x00458E25, sizeof(data), oldProtect, &newProtect);

		// jump to hook, seat is stored in ecx
		injectJump((void*)0x00458E27, &hook_windows);
#else
		injectJumpRBX((void*)0x4ACAB5, (void*)&hook_linux);

#endif

	}
}

