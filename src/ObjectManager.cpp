
#include "ObjectManager.hpp"


namespace ObjectManager
{
#if WIN32
    void (__thiscall *dice_postMessage)(void*, void*, int, float) = (void (__thiscall*)(void*, void*, int, float))0x006E1910;
#define OBJECT_MANAGER ((void**) 0x89AE48) 
#else
    void(*dice_postMessage)(void*, void*, int, float) = (void(*)(void*, void*, int, float))0x0697B10;
#define OBJECT_MANAGER ((void**) 0x112EE48) 
#endif

    void postMessage_py(py::int_ bf2objpointer, py::int_ message, py::float_ time)
    {
#ifdef WIN32
        void* bf2ptr = (void*)(unsigned long)bf2objpointer;
#else
        void* bf2ptr = (void*)(unsigned long long)bf2objpointer;
#endif
        unsigned int bf2Message = message;
        float bf2Time = time;
        postMessage(bf2ptr, bf2Message, bf2Time);
    }

    void postMessage(void* object, int message, float time)
    {
        dice_postMessage(*OBJECT_MANAGER, object, message, time);
    }
}
